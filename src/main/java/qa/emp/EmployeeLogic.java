package qa.emp;

import java.util.List;

public class EmployeeLogic {

    private EmployeeDAO dao;

    public EmployeeLogic(EmployeeDAO dao, String user, String pass) {
        this.dao = dao;
        dao.login(user, pass);
    }

    public double getAverageSalary() {
        List<Employee> allEmployees = dao.getAllEmployees();
        double total = 0.0;
        for (Employee e : allEmployees) {
            total += e.getSalary();
        }
        return total / allEmployees.size();
    }

    public Employee getHighestEarner() {
        List<Employee> allEmployees = dao.getAllEmployees();
        Employee topEmployee = null;
        double highest = 0.0;
        for (Employee e : allEmployees) {
            if (e.getSalary() > highest) {
                highest = e.getSalary();
                topEmployee = e;
            }
        }
        return topEmployee;
    }


}
